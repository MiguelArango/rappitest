package com.example.rappitest.network.response

import com.example.rappitest.model.Movie
import com.squareup.moshi.Json

data class GetPopularMoviesResponse(
    @field:Json(name = "page") val page: Int,
    @field:Json(name = "total_pages") val totalPages: Int,
    @field:Json(name = "total_results") val totalResults: Int,
    @field:Json(name = "results") val movies: List<Movie>
)