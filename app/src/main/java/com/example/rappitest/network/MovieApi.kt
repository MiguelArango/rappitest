package com.example.rappitest.network

import com.example.rappitest.model.Movie
import com.example.rappitest.network.response.GetPopularMoviesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieApi {


    @GET("/3/movie/popular")
    fun getPopularMovies(@Query("api_key") apiKey: String): Observable<GetPopularMoviesResponse>

    @GET("/3/movie/top_rated")
    fun getTopRatedMovies(@Query("api_key") apiKey: String): Observable<List<Movie>>

    @GET("/3/movie/upcoming")
    fun getUpcomingMovies(@Query("api_key") apiKey: String): Observable<List<Movie>>

}