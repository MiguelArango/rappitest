package com.example.rappitest.injection.component

import com.example.rappitest.injection.NetworkModule
import com.example.rappitest.ui.MovieListViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {

    fun inject(movieListViewModel: MovieListViewModel)

    @Component.Builder
    interface Builder{
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}