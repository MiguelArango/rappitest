package com.example.rappitest.injection

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.example.rappitest.model.database.AppDatabase
import com.example.rappitest.ui.MovieListViewModel
import com.example.rappitest.utils.DATABASE_NAME
import java.lang.IllegalArgumentException

class ViewModelFactory(private val activity: AppCompatActivity): ViewModelProvider.Factory {

    override fun <T: ViewModel> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(MovieListViewModel::class.java)){
            val db = Room.databaseBuilder(activity.applicationContext, AppDatabase::class.java, DATABASE_NAME).build()
            @Suppress("UNCHECKED_CAST")
            return MovieListViewModel(db.movieDao()) as T
        }
        throw IllegalArgumentException("Unknow view class")
    }
}