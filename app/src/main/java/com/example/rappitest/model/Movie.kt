package com.example.rappitest.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import retrofit2.http.Field
import java.io.Serializable

@Entity
data class Movie(
    @field:PrimaryKey @Json(name = "id") val id: Int,
    @Json(name = "title") val title: String,
    @Json(name = "overview") val overview: String,
    @field:Json(name = "original_title") val originalTitle: String,
    @Json(name = "video") val video: Boolean,
    @field:Json(name = "vote_count") val voteCount: Int,
    @field:Json(name = "poster_path") val posterPath: String,
    @field:Json(name = "backdrop_path") val backdropPath: String,
    @field:Json(name = "vote_average") val voteAverage: Double,
    @Json(name = "adult") val adult: Boolean,
    @field:Json(name = "release_date") val releaseDate: String
) : Serializable
