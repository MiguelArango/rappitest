package com.example.rappitest.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.rappitest.model.Movie
import com.example.rappitest.model.MovieDao

@Database(entities = arrayOf(Movie::class),
    version = 1)
abstract class AppDatabase: RoomDatabase() {

    abstract fun movieDao(): MovieDao
}