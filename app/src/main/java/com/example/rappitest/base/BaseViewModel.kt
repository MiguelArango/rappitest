package com.example.rappitest.base

import androidx.lifecycle.ViewModel
import com.example.rappitest.injection.NetworkModule
import com.example.rappitest.injection.component.DaggerViewModelInjector
import com.example.rappitest.injection.component.ViewModelInjector
import com.example.rappitest.ui.MovieListViewModel

abstract class BaseViewModel : ViewModel() {

    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init{
        inject()
    }

    private fun inject(){
        when(this){
            is MovieListViewModel -> injector.inject(this);
        }
    }
}