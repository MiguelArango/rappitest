package com.example.rappitest.ui.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.rappitest.R
import com.example.rappitest.databinding.ItemMovieBinding
import com.example.rappitest.model.Movie
import com.example.rappitest.ui.movie.detail.MovieDetailActivity


class MovieListAdapter: RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {

    private lateinit var movieList:List<Movie>


    override fun getItemCount(): Int {
        return if(::movieList.isInitialized) movieList.size else 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(movieList[position])
        holder.itemView.setOnClickListener {
                v ->

                    val intent = MovieDetailActivity.newIntent(v.context, movieList[position])
                    v.context.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemMovieBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_movie, parent, false)
        return ViewHolder(binding)
    }

    fun updateMovieList(movieList:List<Movie>){
        this.movieList = movieList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemMovieBinding):RecyclerView.ViewHolder(binding.root) {
        private val viewModel = MovieViewModel()

        fun bind(movie: Movie){
            viewModel.bind(movie)
            binding.viewModel = viewModel
        }

    }
}