package com.example.rappitest.ui.movie

import androidx.lifecycle.MutableLiveData
import com.example.rappitest.base.BaseViewModel
import com.example.rappitest.model.Movie
import com.example.rappitest.utils.IMAGE_BASE_URL

class MovieViewModel: BaseViewModel() {

    private val title = MutableLiveData<String>()
    private val posterPath = MutableLiveData<String>()

    fun bind(movie: Movie){
        title.value = movie.title
        posterPath.value = IMAGE_BASE_URL + movie.posterPath
    }



    fun getTitle(): MutableLiveData<String>{
        return title
    }

    fun getPosterPath(): MutableLiveData<String>{
        return posterPath
    }
}