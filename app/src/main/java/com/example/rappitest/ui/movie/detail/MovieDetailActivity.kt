package com.example.rappitest.ui.movie.detail

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.rappitest.R
import com.example.rappitest.utils.IMAGE_BASE_URL
import com.example.rappitest.utils.IMAGE_BASE_URL_HIGH
import kotlinx.android.synthetic.main.activity_movie_detail.*
import android.content.Intent
import android.content.Context
import com.example.rappitest.model.Movie
import kotlinx.android.synthetic.main.content_movie_detail.*


class MovieDetailActivity : AppCompatActivity() {


    companion object {

        private val INTENT_MOVIE = "movie"

        fun newIntent(context: Context, movie: Movie): Intent {
            val intent = Intent(context, MovieDetailActivity::class.java)
            intent.putExtra(INTENT_MOVIE, movie)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)


        cvBack.setOnClickListener { v->
            finish()
        }


        val currentMovie =  getIntent().getSerializableExtra(INTENT_MOVIE)
        if (currentMovie == null) {
            throw IllegalStateException("field " + INTENT_MOVIE + " missing in Intent");
        }

        updateUI(currentMovie as Movie)
    }

    private fun updateUI(movie: Movie){


        tvOverview.text = movie.overview
        tvTitle.text = movie.title
        tvVoteCount.text = movie.voteCount.toString() + " Votos"
        tvReleaseDate.text = "Estreno en: " + movie.releaseDate
        tvVoteAverage.text = movie.voteAverage.toString()


        Glide.with(this)
            .load(IMAGE_BASE_URL_HIGH + movie.backdropPath).apply(RequestOptions())
            .into(ivBackdrop)


    }
}
