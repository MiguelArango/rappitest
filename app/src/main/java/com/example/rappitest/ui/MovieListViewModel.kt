package com.example.rappitest.ui

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.example.rappitest.R
import com.example.rappitest.base.BaseViewModel
import com.example.rappitest.model.Movie
import com.example.rappitest.model.MovieDao
import com.example.rappitest.network.MovieApi
import com.example.rappitest.network.response.GetPopularMoviesResponse
import com.example.rappitest.ui.movie.MovieListAdapter
import com.example.rappitest.utils.MOVIE_API_KEY
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MovieListViewModel(private val movieDao: MovieDao) : BaseViewModel() {

    @Inject lateinit var movieApi: MovieApi

    private lateinit var subscription: Disposable

    val movieListAdapter: MovieListAdapter = MovieListAdapter()

    val errorMessage:MutableLiveData<Int> = MutableLiveData()
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadMovies() }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    init{
        loadMovies()
    }

    private fun loadMovies() {

        /*
        subscription = Observable.fromCallable { movieDao.all }
            .concatMap {
                dbMovieList ->
                    if(dbMovieList.isEmpty()){
                        movieApi.getPopularMovies(MOVIE_API_KEY).concatMap {
                            response ->
                                movieDao.insertAll(response.movies.toTypedArray().toList())

                            Observable.just(response.movies)
                        }
                    }   else{
                        Observable.just(dbMovieList)
                    }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe( onRetrieveMovieListStart())
            .doOnTerminate( onRetrieveMovieListFinish())
            .subscribe(
                {   result -> onRetrieveMovieListSuccess(result) },
                { onRetrieveMovieListError()}
            )
            */




        subscription = movieApi.getPopularMovies(MOVIE_API_KEY)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe{onRetrieveMovieListStart()}
            .doOnTerminate{onRetrieveMovieListFinish()}
            .subscribe(
                {   result -> onRetrieveMovieListSuccess(result)},
                {onRetrieveMovieListError()}
            )


    }

    private fun onRetrieveMovieListStart(){
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveMovieListFinish(){
        loadingVisibility.value = View.GONE

    }


    private fun onRetrieveMovieListSuccess(movies: List<Movie>?){

        if(movies != null){
            if(!movies.isEmpty()){
                movieListAdapter.updateMovieList(movies)
            }
        }

    }

    private fun onRetrieveMovieListSuccess(getPopularMoviesResponse: GetPopularMoviesResponse){

        if(getPopularMoviesResponse.movies != null){
            movieListAdapter.updateMovieList(getPopularMoviesResponse.movies)
        }

    }

    private fun onRetrieveMovieListError(){
        errorMessage.value = R.string.get_movies_error
    }
}